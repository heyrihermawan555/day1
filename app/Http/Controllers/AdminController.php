<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //
	public function home(){
		return view('indexadmin');
	}
	public function listproduct(){
		$barangs = DB::table('barangs')->get();

    	// mengirim data pegawai ke view home
		return view('listproduct')->with('barangs',$barangs);	
	}
	public function listtype(){
		$jenis = DB::table('jenis')->get();

    	// mengirim data pegawai ke view home
		return view('listtype')->with('jenis',$jenis);	
	}
	public function customer(){
		$users = DB::table('users')->get();

    	// mengirim data pegawai ke view home
		return view('customer')->with('users',$users);	
	}
	public function transaction(){
		$pesanans = DB::table('pesanans')->get();

    	// mengirim data pegawai ke view home
		return view('transaction')->with('pesanans',$pesanans);	
	}
	public function tambahpro(){
		$jenis = DB::table('jenis')->get();
		return view('tambahproduk')->with('jenis',$jenis);	
	}

	public function tambahjen(){
		return view('tambahjenis');
	}

	
	//=================CRUD=================//

	public function Update(Request $request){

		DB::table('barangs')->where('id',$request->id)->update([
			'nama_barang' => $request->nama_barang,
			'jenis_barang' => $request->jenis_barang,
			'harga' => $request->harga,
			'stok' => $request->stok,
			'keterangan' => $request->keterangan
		]);
		return redirect('/listp');


/*		$oldlogo = $request->old_logo;
		$data = array();
		$data['nama_barang'] = $request->nama_barang;
		$data['jenis_barang'] = $request->jenis_barang;
		$data['harga'] = $request->harga;
		$data['stok'] = $request->stok;
		$data['keterangan'] = $request->keterangan;

		$image = $request->file('foto');
		if ($image) 
		{
			unlink($oldlogo);
			$image_name = date('dmy_H_s_i');
			$ext = strtolower($image->getClientOriginalExtension());
			$image_full_name = $image_name.'.'.$ext;
			$upload_path = 'img/produk/';
			$image_url = $upload_path.$image_full_name;
			$success = $image->move($upload_path,$image_full_name);

		$data['foto'] = $image_full_name;
		$product = DB::table('barangs')->where('id', $id)->update($data);
		return redirect()->route('product.index');

	}*/
}	


public function StoreJ(Request $request){
	$data = array();		
	$data['jenis_barang'] = $request->jenis_barang;		
	$product = DB::table('jenis')->insert($data);
	return redirect('listty');


}

public function Edit($id){
	$barangs = DB::table('barangs')->where('id',$id)->get();	
	return view('editproduct')->with('barangs',$barangs);

}	

public function Store(request $request){
	$data = array();
	$data['nama_barang'] = $request->nama_barang;
	$data['jenis_barang'] = $request->jenis_barang;
	$data['harga'] = $request->harga;
	$data['stok'] = $request->stok;
	$data['keterangan'] = $request->keterangan;

	$image = $request->file('foto');
	if ($image) 
	{
		$image_name = date('dmy_H_s_i');
		$ext = strtolower($image->getClientOriginalExtension());
		$image_full_name = $image_name.'.'.$ext;
		$upload_path = 'img/produk/';
		$image_url = $upload_path.$image_full_name;
		$success = $image->move($upload_path,$image_full_name);

		$data['foto'] = $image_full_name;
		$product = DB::table('barangs')->insert($data);
		return redirect('listp');

	}

}
public function hapus($id)
{
		// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('barangs')->where('id',$id)->delete();

		// alihkan halaman ke halaman pegawai
	return redirect('listp');
}


public function hapusni($id)
{
		// menghapus data pegawai berdasarkan id yang dipilih
	DB::table('jenis')->where('id',$id)->delete();

		// alihkan halaman ke halaman pegawai
	return redirect('listty');

}

}




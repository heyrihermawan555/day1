<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Pesanan;
use App\PesananDetail;
use App\User;
use Auth;
use Alert;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    //
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function home()
	{
    	// mengambil data dari table produk
		$barangs = DB::table('barangs')->get();

    	// mengirim data pegawai ke view home
		return view('home')->with('barangs',$barangs);


	}

	public function detail($id)
	{
	// mengambil data pegawai berdasarkan id yang dipilih
		$barangs = DB::table('barangs')->where('id',$id)->get();
	// passing data pegawai yang didapat ke view edit.blade.php
		return view('detail')->with('barangs',$barangs);

	}

	public function pesan(Request $request ,$id)
	{
		$barang = barang::where('id', $id)->first();
		$tanggal = Carbon::now();

		if ($request->jumlah_pesan > $barang->stok)
		{
			return redirect('zen/detail/'.$id);
		}

		$cek_pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
		if (empty($cek_pesanan)) 
		{
			$pesanan = new pesanan;
			$pesanan->user_id = Auth::user()->id;
			$pesanan->tanggal = $tanggal;
			$pesanan->status = 0;
			$pesanan->jumlah_harga = 0;
			$pesanan->kode = mt_rand(100, 999);
			$pesanan->save();
		}

		

		$pesanan_baru = Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();

		$cek_pesanan_detail = PesananDetail::where('barang_id', $barang->id)->where('pesanan_id',$pesanan_baru->id)->first();
		if (empty($cek_pesanan_detail)) 
		{
			$pesanan_detail = new PesananDetail;
			$pesanan_detail->barang_id = $barang->id;
			$pesanan_detail->pesanan_id = $pesanan_baru->id;
			$pesanan_detail->jumlah = $request->jumlah_pesan;
			$pesanan_detail->jumlah_harga = $barang->harga*$request->jumlah_pesan;
			$pesanan_detail->save();
		}
		else
		{
			$pesanan_detail = PesananDetail::where('barang_id',$barang->id)->where('pesanan_id' ,$pesanan_baru->id)->first();
			$pesanan_detail->jumlah = $pesanan_detail->jumlah+$request->jumlah_pesan;
			$harga_pesanan_detail_baru = $barang->harga*$request->jumlah_pesan;
			$pesanan_detail->jumlah_harga = $pesanan_detail->jumlah_harga+$harga_pesanan_detail_baru;
			$pesanan_detail->update();
		}

		$pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
		$pesanan->jumlah_harga = $pesanan->jumlah_harga+$barang->harga*$request->jumlah_pesan;
		$pesanan->update();

		Alert::success('Product add to cart', 'Success');
		return redirect('check-out');
	}

	public function check_out()
	{
		$pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
		if(!empty($pesanan)) 
		{
		$pesanan_detail = PesananDetail::where('pesanan_id', $pesanan->id)->get();		
		return view('check_out', compact('pesanan' ,'pesanan_detail'));
		}
	


	}
	public function delete($id)
	{
		$pesanan_detail = PesananDetail::where('id',$id)->first();

		$pesanan = Pesanan::where('id', $pesanan_detail->pesanan_id)->first();
		$pesanan->jumlah_harga = $pesanan->jumlah_harga-$pesanan_detail->jumlah_harga;
		$pesanan->update();

		$pesanan_detail->delete();

		Alert::error('Product Deleted', "Delete");
		return redirect('check-out');
	}

	public function konfirmasi()
    {
        $user = User::where('id', Auth::user()->id)->first();

        if(empty($user->alamat))
        {
            Alert::error('Identitasi Harap dilengkapi', 'Error');
            return redirect('profile');
        }

        if(empty($user->nohp))
        {
            Alert::error('Identitasi Harap dilengkapi', 'Error');
            return redirect('profile');
        }

        $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
        $pesanan_id = $pesanan->id;
        $pesanan->status = 1;
        $pesanan->update();

        $pesanan_details = PesananDetail::where('pesanan_id', $pesanan_id)->get();
        foreach ($pesanan_details as $pesanan_detail) {
            $barang = Barang::where('id', $pesanan_detail->barang_id)->first();
            $barang->stok = $barang->stok-$pesanan_detail->jumlah;
            $barang->update();
        }



        Alert::success('Pesanan Sukses Check Out Silahkan Lanjutkan Proses Pembayaran', 'Success');
        return redirect('history/'.$pesanan_id);

    }





}

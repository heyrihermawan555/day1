<!-- Menghubungkan dengan view template master -->
@extends('master')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->



<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('konten')

<div id="about" class="about-area area-padding">
	<div class="col-md-12">
		<div class="thumbnail"><br>
			<h3><i class="fa fa-shopping-cart"></i> Checkout</h3><br><br>
			@if(!empty($pesanan))
			<strong>Tanggan Pesan : {{$pesanan->tanggal}} </strong>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Barang</th>
						<th>Jumlah</th>
						<th>Harga</th>
						<th>Total Harga</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1 ?>
						@foreach($pesanan_detail as $pesanan_detail)
					<tr>						
						<td>{{ $no++ }} </td>
						<td>{{ $pesanan_detail->barang->nama_barang }}</td>
						<td>{{ $pesanan_detail->jumlah }}</td>
						<td align="left">Rp. {{number_format ($pesanan_detail->barang->harga) }}</td>
						<td align="left">Rp. {{number_format ($pesanan_detail->jumlah_harga)  }}</td>
						<td>
							<form action="{{ url('check-out')}}/{{$pesanan_detail->id}} " method="post">
								@csrf
								{{method_field('DELETE')}}
								<button type="submit" class="btn btn-danger"><li class="fa fa-trash"></li></button>
							</form>
						</td>
					</tr>
						@endforeach
						<tr>
							<td colspan="4" align="left"><strong>Total Harga :</strong></td>
							<td><strong>Rp. {{number_format($pesanan->jumlah_harga)}}</strong> </td><br><br>
							<td></td>
						</tr>
				</tbody>
			</table>
				@endif
				<a href="{{url('konfirmasi-check-out')}} " class="btn btn-success"><li class="fa fa-shopping-cart"></li> Checkout</a><br><br>
		</div>
	</div>
</div>
@endsection
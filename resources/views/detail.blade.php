<!-- Menghubungkan dengan view template master -->
@extends('master')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->



<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('konten')

<div id="about" class="about-area area-padding">
	<div class="container">
		<div class="thumbnail"><br><br>
			@foreach($barangs as $p)
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">					
				</div>
				<div class="col-md-6">
					<img src="{{asset('img/produk')}}//{{ $p->foto }} " width="400">
				</div>
				<div class="col-md-6 mt-5">					
					<h2><?php echo $p->nama_barang ?></h2>					
					<table class="table">
						<tbody>
							<tr>
								<td>Harga</td>
								<td>:</td>
								<td>Rp.{{ number_format($p->harga)}} </td>
							</tr>
							<tr>
								<td>Stok</td>
								<td>:</td>
								<td>{{$p->stok}} </td>
							</tr>
							<tr>
								<td>Detail</td>
								<td>:</td>
								<td>{{$p->keterangan}} </td>
							</tr>
							<tr>
								<td>Jumlah Pesan</td>
								<td>:</td>
								<td>
									<div class="form-group" id="only-number">
										<form method="post" action="{{ url('zen/pesan')}}/{{$p->id}} ">				@csrf	
											<input type="text" name="jumlah_pesan" class="form-control" id="number" placeholder="Input only number" onkeypress="return hanyaAngka(event)" required=""><br>
											<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-shopping-cart"></i>  Add to Cart</button>
										</form>
									</div>
								</td>
							</tr>																
						</tbody>
					</table>
				</div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			</div>
		</div>
		@endforeach
	</div>
</div>

@endsection
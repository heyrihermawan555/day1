<!-- Menghubungkan dengan view template master -->
@extends('masteradmin')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->



<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('admin')



  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        @foreach($barangs as $p)
        <form action="/updateproduct" method="POST" enctype="multipart/form-data">
           @csrf
          <input type="hidden" name="id" value="{{ $p->id }}">
          <div class="form-group">
            <label>Nama Produk</label>
            <input type="text" class="form-control" name="nama_barang" width="20px" value="{{$p->nama_barang}} ">
          </div>
          <div class="form-group">
            <label>Jenis Barang</label>       
            <input type="text" class="form-control" name="jenis_barang" width="20px" value="{{$p->jenis_barang}} ">        
          </div>
          <div class="form-group">
            <label>Harga</label>
            <input type="text" class="form-control" name="harga" width="20px" value="{{$p->harga}} ">
          </div>
          <div class="form-group">
            <label>Stok Barang</label>
            <input type="text" class="form-control" name="stok" width="20px" value="{{$p->stok}} ">
          </div>    
          <div class="form-group">
            <label>Keterangan</label>        
            <textarea type="text" class="form-control" name="keterangan" width="20px">{{$p->keterangan}}</textarea>
          </div>
          <div class="form-group">
            <label>Foto</label><br>
            <img src="{{asset('img/produk')}}//{{ $p->foto }} " width="400"><br><br>
            <input type="hidden" class="form-control" name="old_logo" value="{{ $p->foto }}">
          </div><br>
          <button class=" btn btn-primary" type="submit">Save</button>
          <br><br>
        </form>
        <!-- /.row -->
        <!-- Main row -->

        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endforeach

  @endsection
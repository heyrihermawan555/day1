<!-- Menghubungkan dengan view template master -->
@extends('master')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->



<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('konten')

<div id="about" class="about-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>About eBusiness</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($barangs as $p)
            <div class="col-md-4">
                <div class="thumbnail">
                  <img src="{{asset('img/produk')}}//{{ $p->foto }} " >
                  <div class="caption text-center">
                    <h4 style="font-weight: bold;"><?php echo $p->nama_barang ?></h4>
                    <p>Stok : {{ $p->stok}} </p>
                    <p>Rp.{{ number_format($p->harga)}} </p>
                    <p>                        
                      <a href="/zen/detail/{{ $p->id }}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy Now</a>
                  </p>
              </div>      
          </div>
      </div>
      @endforeach
  </div>
</div>
</div>

@endsection
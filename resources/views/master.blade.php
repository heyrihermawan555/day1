<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zen Store</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{asset('img/favicon.png')}}" rel="icon">
  <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="{{asset('https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900')}}" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('lib/nivo-slider/css/nivo-slider.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/owl.transitions.css')}}" rel="stylesheet">
  <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/venobox/venobox.css')}}" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="{{asset('css/nivo-slider-theme.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
    
  </head>

  <body data-spy="scroll" data-target="#navbar-example">

    <div id="preloader"></div>

    <header>
      <!-- header-area start -->
      <div id="sticker" class="header-area">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12">

              <!-- Navigation -->
              <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <!-- Brand -->
                  <a class="navbar-brand page-scroll sticky-logo" href="/">
                    <h1><span>Zen</span>Store</h1>
                    <!-- Uncomment below if you prefer to use an image logo -->
                    <!-- <img src="img/logo.png" alt="" title=""> -->
                  </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                  <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                      <a class="page-scroll" href="/zen">Home</a>
                    </li>                                     
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Product<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href=# >Laptop</a></li>
                        <li><a href=# >Notebook</a></li>
                        <li><a href=# >Macbook</a></li>
                      </ul> 
                    </li>                    
                    <li>
                      <?php 
                      $pesanan_utama = \App\Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
                      if(!empty($pesanan_utama))
                      {                      
                      $notif = \App\PesananDetail::where('pesanan_id', $pesanan_utama->id)->count();
                      }
                       ?>
                      <a class="page-scroll" href="{{ url('check-out') }}"><i class="fa fa-shopping-cart"></i>
                        @if(!empty($notif))
                        <span class="badge badge-danger">{{ $notif }}</span>
                        @endif
                      </a>
                    </li>
                    <li>
                      <a class="page-scroll" href="#services">Services</a>
                    </li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Account<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('profile')}} " >Profile</a></li>
                        <li><a href="{{ url('history')}} " >History</a></li>
                       <li>
                        <a class="page-scroll" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                      </form>        
                    </li>
                  </ul> 
                </li>

              </ul>
            </div>
            <!-- navbar-collapse -->
          </nav>
          <!-- END: Navigation -->
        </div>
      </div>
    </div>
  </div>
  <!-- header-area end -->
</header>
<!-- header end -->

<!-- Start Slider Area -->
<div id="home" class="slider-area">
  <div class="bend niceties preview-2">
    <div id="ensign-nivoslider" class="slides">
      <img src="{{asset('img/slider/slider1.jpg')}}" alt="" title="#slider-direction-1" />
      <img src="{{asset('img/slider/slider2.jpg')}}" alt="" title="#slider-direction-2" />
      <img src="{{asset('img/slider/slider3.jpg')}}" alt="" title="#slider-direction-3" />
    </div>

    <!-- direction 1 -->
    <div id="slider-direction-1" class="slider-direction slider-one">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="slider-content">
              <!-- layer 1 -->
              <div class="layer-1-1 hidden-xs wow slideInDown" data-wow-duration="2s" data-wow-delay=".2s">
                <h2 class="title1">The Best Computer Store </h2>
              </div>
              <!-- layer 2 -->
              <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                <h1 class="title2">We Help You Find The Best</h1>
              </div>
              <!-- layer 3 -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- direction 2 -->
  <div id="slider-direction-2" class="slider-direction slider-two">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">
            <!-- layer 1 -->
            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
              <h2 class="title1">The Best Computer Store </h2>
            </div>
            <!-- layer 2 -->
            <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
              <h1 class="title2">Upgrade Your Machine</h1>
            </div>
            <!-- layer 3 -->
            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- direction 3 -->
  <div id="slider-direction-3" class="slider-direction slider-two">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content">
            <!-- layer 1 -->
            <div class="layer-1-1 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
              <h2 class="title1">The Best Computer Store </h2>
            </div>
            <!-- layer 2 -->
            <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
              <h1 class="title2">Helping Business Security  & Peace of Mind for Your Family</h1>
            </div>
            <!-- layer 3 -->
            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">               
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Slider Area -->

<!-- Start About area -->
<!-- bagian judul halaman blog -->
<!-- bagian konten blog -->
@yield('konten')
<!-- End About area -->
<br><br><br><br><br><br><br><br>
<!-- Start Service area -->
<div id="services" class="services-area area-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline services-head text-center">

          <!-- End Contact Area -->

          <!-- Start Footer bottom Area -->
          <footer>
            <div class="footer-area">
              <div class="container">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-content">
                      <div class="footer-head">
                        <div class="footer-logo">
                          <h2><span>Zen</span>Store</h2>
                        </div>

                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>
                        <div class="footer-icons">
                          <ul>
                            <li>
                              <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                              <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                              <a href="#"><i class="fa fa-google"></i></a>
                            </li>
                            <li>
                              <a href="#"><i class="fa fa-pinterest"></i></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single footer -->
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-content">
                      <div class="footer-head">
                        <h4>information</h4>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                        </p>
                        <div class="footer-contacts">
                          <p><span>Tel:</span> +123 456 789</p>
                          <p><span>Email:</span> contact@example.com</p>
                          <p><span>Working Hours:</span> 9am-5pm</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single footer -->
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-content">
                      <div class="footer-head">
                        <h4>Instagram</h4>
                        <div class="flicker-img">
                          <a href="#"><img src="img/portfolio/1.jpg" alt=""></a>
                          <a href="#"><img src="img/portfolio/2.jpg" alt=""></a>
                          <a href="#"><img src="img/portfolio/3.jpg" alt=""></a>
                          <a href="#"><img src="img/portfolio/4.jpg" alt=""></a>
                          <a href="#"><img src="img/portfolio/5.jpg" alt=""></a>
                          <a href="#"><img src="img/portfolio/6.jpg" alt=""></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-area-bottom">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="copyright text-center">
                      <p>
                        &copy; Copyright <strong>eBusiness</strong>. All Rights Reserved
                      </p>
                    </div>
                    <div class="credits">
              <!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eBusiness
              -->
              Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('lib/venobox/venobox.min.js')}}"></script>
  <script src="{{asset('lib/knob/jquery.knob.js')}}"></script>
  <script src="{{asset('lib/wow/wow.min.js')}}"></script>
  <script src="{{asset('lib/parallax/parallax.js')}}"></script>
  <script src="{{asset('lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('lib/nivo-slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
  <script src="{{asset('lib/appear/jquery.appear.js')}}"></script>
  <script src="{{asset('lib/isotope/isotope.pkgd.min.js')}}"></script>

  <!-- Contact Form JavaScript File -->
  <script src="{{asset('contactform/contactform.js')}}"></script>

  <script src="{{asset('js/main.js')}}"></script>
  <script src="{{asset('js/number.js')}}"></script>
</body>

</html>

 <!-- Menghubungkan dengan view template master -->
@extends('masteradmin')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->



<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('admin')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Add product</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v1</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <form action="{{route('product.store')}} " method="POST" enctype="multipart/form-data">
     @csrf
      <div class="form-group">
        <label>Nama Produk</label>
        <input type="text" class="form-control" name="nama_barang" width="20px">
      </div>
      <div class="form-group">
        <label>Jenis Barang</label>
       <select class="form-control" name="jenis_barang">
          <option value="">Pilih Jenis</option>
          @foreach($jenis as $p)            
          <option value="{{$p->jenis_barang}}">{{$p->jenis_barang}}</option>        
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label>Harga</label>
        <input type="text" class="form-control" name="harga" width="20px">
      </div>
      <div class="form-group">
        <label>Stok Barang</label>
        <input type="text" class="form-control" name="stok" width="20px">
      </div>    
      <div class="form-group">
        <label>Keterangan</label>        
        <textarea type="text" class="form-control" name="keterangan" width="20px"></textarea>
      </div>
      <div class="form-group">
        <label>Foto</label>
        <input type="file" class="form-control" name="foto">
      </div><br>
      <button class=" btn btn-primary" type="submit">Save</button>
      <br><br>
      
    </form>
      <!-- /.row -->
      <!-- Main row -->

      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
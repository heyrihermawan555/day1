<!-- Menghubungkan dengan view template master -->
@extends('masteradmin')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->



<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('admin')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Transactions</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard v1</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Total Harga</th>
            <th>Tanggal</th>              
          </tr>
        </thead>
        <tbody>
          <?php $no = 1 ?>
          @foreach($pesanans as $p)
          <tr>
            <td>{{ $no++ }}</td>              
            <td>Rp.{{ number_format($p->jumlah_harga)}}</td>                        
            <td>{{$p->tanggal}}</td>    
          </tr>
          @endforeach
        </tbody>
      </table>
      <!-- /.row -->
      <!-- Main row -->

      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
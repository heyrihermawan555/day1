<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Zen Store</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="" name="keywords">
	<meta content="" name="description">

	<!-- Favicons -->
	<link href="img/favicon.png" rel="icon">
	<link href="img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

	<!-- Bootstrap CSS File -->
	<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Libraries CSS Files -->
	<link href="lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
	<link href="lib/owlcarousel/owl.carousel.css" rel="stylesheet">
	<link href="lib/owlcarousel/owl.transitions.css" rel="stylesheet">
	<link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="lib/animate/animate.min.css" rel="stylesheet">
	<link href="lib/venobox/venobox.css" rel="stylesheet">

	<!-- Nivo Slider Theme -->
	<link href="css/nivo-slider-theme.css" rel="stylesheet">

	<!-- Main Stylesheet File -->
	<link href="css/style.css" rel="stylesheet">

	<!-- Responsive Stylesheet File -->
	<link href="css/responsive.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
</head>

<body data-spy="scroll" data-target="#navbar-example">

	<div id="preloader"></div>

	<header>
		<!-- header-area start -->
		<div class="container"><br><br>
			<div class="thumbnail">
				<br>	
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<div align="center">
								<img src="{{asset('img/admin.jpg')}}" width="400"><br><br>	
								<div class="section-headline text-center">
									<h2>Admin</h2>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div align="center">
								<a href="/zen"><img src="{{asset('img/user.jpg')}}" width="400"></a><br>	
								<div class="section-headline text-center">
									<h2>Users</h2>
								</div>
							</div>
						</div>
					</div>
				</div>		
			</div>
		</div>


		<!-- End Contact Area -->

		<!-- Start Footer bottom Area -->


		<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

		<!-- JavaScript Libraries -->
		<script src="lib/jquery/jquery.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		<script src="lib/owlcarousel/owl.carousel.min.js"></script>
		<script src="lib/venobox/venobox.min.js"></script>
		<script src="lib/knob/jquery.knob.js"></script>
		<script src="lib/wow/wow.min.js"></script>
		<script src="lib/parallax/parallax.js"></script>
		<script src="lib/easing/easing.min.js"></script>
		<script src="lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
		<script src="lib/appear/jquery.appear.js"></script>
		<script src="lib/isotope/isotope.pkgd.min.js"></script>

		<!-- Contact Form JavaScript File -->
		<script src="contactform/contactform.js"></script>

		<script src="js/main.js"></script>
	</body>

	</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('tes', function () {
	return view('tes');
});

//Customer

Route::get('/zen', 'WebsController@home');
Route::get('/zen/kontak', 'WebsController@kontak');

Route::get('/zen','ProdukController@home');
Route::get('/zen/detail/{id}', 'ProdukController@detail');
Route::post('/zen/pesan/{id}', 'ProdukController@pesan');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('check-out', 'ProdukController@check_out');
Route::delete('check-out/{id}', 'ProdukController@delete');
Route::get('konfirmasi-check-out', 'ProdukController@konfirmasi');

Route::get('profile', 'ProfileController@index');
Route::post('profile', 'ProfileController@update');

Route::get('history', 'HistoryController@index');
Route::get('history/{id}', 'HistoryController@detail');



//Admin
Route::get('/admin', 'AdminController@home');
Route::get('/listp', 'AdminController@listproduct');
Route::get('/listty', 'AdminController@listtype');
Route::get('/customer', 'AdminController@customer');
Route::get('/transaction', 'AdminController@transaction');
Route::get('/tambahpro', 'AdminController@tambahpro');
Route::get('/tambahjen', 'AdminController@tambahjen');


//CRUD
Route::post('store', 'AdminController@Store')->name('product.store');
Route::post('/listp', 'AdminController@listproduct')->name('product.index');
Route::post('storej', 'AdminController@StoreJ')->name('jenis.store');
Route::get('editproduct/{id}', 'AdminController@Edit');

Route::post('/updateproduct', 'AdminController@Update');

Route::get('hapusproduk/{id}','AdminController@hapus');

Route::get('hapustype/{id}','AdminController@hapusni');
